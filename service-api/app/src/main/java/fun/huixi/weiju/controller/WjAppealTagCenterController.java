package fun.huixi.weiju.controller;


import fun.huixi.weiju.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * 诉求与标志的对应关系 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-10
 */
@RestController
@RequestMapping("/wjAppealTagCenter")
public class WjAppealTagCenterController extends BaseController {

}

