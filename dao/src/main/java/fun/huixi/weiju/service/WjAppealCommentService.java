package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.dto.appeal.SaveAppealCommentDTO;
import fun.huixi.weiju.pojo.entity.WjAppealComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诉求-评论 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealCommentService extends IService<WjAppealComment> {


    /**
     *  保存诉求评论
     * @Author 叶秋
     * @Date 2021/11/16 11:39
     * @param userId 用户id
     * @param saveAppealCommentDTO 保存参数
     * @return void
     **/
    void saveAppealComment(Integer userId, SaveAppealCommentDTO saveAppealCommentDTO);
}
