package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjAppeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealMapper extends BaseMapper<WjAppeal> {

}
