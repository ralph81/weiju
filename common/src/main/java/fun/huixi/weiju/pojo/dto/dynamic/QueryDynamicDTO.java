package fun.huixi.weiju.pojo.dto.dynamic;

import fun.huixi.weiju.page.PageQuery;
import lombok.Data;

/**
 *  动态查询DTO
 * @Author Zol
 * @Date 2021/11/10
 * @param 
 * @return 
 **/
@Data
public class QueryDynamicDTO extends PageQuery{

    /**
     * 动态标签，对应的字典id
     */
    private Integer tagDictId;

    /**
     * 是否主页
     */
    private Integer isHome;

}
