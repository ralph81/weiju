package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态评论表
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_dynamic_comment")
public class WjDynamicComment extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 动态评论id
     */
    @TableId("dynamic_comment_id")
    private Integer dynamicCommentId;

    /**
     * 对应的动态id
     */
    @TableField("dynamic_id")
    private Integer dynamicId;

    /**
     * 发起评论的用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 评论的内容
     */
    @TableField("content")
    private String content;

    /**
     * 评论的点赞数
     */
    @TableField("endorse_count")
    private Integer endorseCount;

    /**
     * 评价评论的次数
     */
    @TableField("comment_count")
    private Integer commentCount;


}
