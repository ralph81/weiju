package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求-评论
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal_comment")
public class WjAppealComment extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 诉求评论的id
     */
    @TableId(value = "appeal_comment_id", type = IdType.AUTO)
    private Integer appealCommentId;

    /**
     * 诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 评论人的id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 评论的内容
     */
    @TableField("content")
    private String content;

    /**
     * 评论所加的贴图（只能是一张）
     */
    @TableField("url")
    private String url;

    /**
     * 评论的点赞数||赞同数
     */
    @TableField("endorse_count")
    private Integer endorseCount;

    /**
     * 对评论评论的次数
     */
    @TableField("comment_count")
    private Integer commentCount;



}
